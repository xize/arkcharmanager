﻿namespace ArkCharManager
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.checkBox42 = new System.Windows.Forms.CheckBox();
            this.checkBox41 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.button5 = new System.Windows.Forms.Button();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button4 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.checkBox43 = new System.Windows.Forms.CheckBox();
            this.checkBox44 = new System.Windows.Forms.CheckBox();
            this.checkBox45 = new System.Windows.Forms.CheckBox();
            this.checkBox46 = new System.Windows.Forms.CheckBox();
            this.checkBox47 = new System.Windows.Forms.CheckBox();
            this.checkBox48 = new System.Windows.Forms.CheckBox();
            this.checkBox49 = new System.Windows.Forms.CheckBox();
            this.checkBox50 = new System.Windows.Forms.CheckBox();
            this.checkBox51 = new System.Windows.Forms.CheckBox();
            this.checkBox52 = new System.Windows.Forms.CheckBox();
            this.button6 = new System.Windows.Forms.Button();
            this.checkBox53 = new System.Windows.Forms.CheckBox();
            this.checkBox54 = new System.Windows.Forms.CheckBox();
            this.checkBox55 = new System.Windows.Forms.CheckBox();
            this.checkBox56 = new System.Windows.Forms.CheckBox();
            this.checkBox57 = new System.Windows.Forms.CheckBox();
            this.checkBox58 = new System.Windows.Forms.CheckBox();
            this.checkBox59 = new System.Windows.Forms.CheckBox();
            this.checkBox60 = new System.Windows.Forms.CheckBox();
            this.checkBox61 = new System.Windows.Forms.CheckBox();
            this.checkBox62 = new System.Windows.Forms.CheckBox();
            this.checkBox63 = new System.Windows.Forms.CheckBox();
            this.checkBox64 = new System.Windows.Forms.CheckBox();
            this.checkBox65 = new System.Windows.Forms.CheckBox();
            this.checkBox66 = new System.Windows.Forms.CheckBox();
            this.checkBox67 = new System.Windows.Forms.CheckBox();
            this.checkBox68 = new System.Windows.Forms.CheckBox();
            this.checkBox69 = new System.Windows.Forms.CheckBox();
            this.checkBox70 = new System.Windows.Forms.CheckBox();
            this.checkBox71 = new System.Windows.Forms.CheckBox();
            this.checkBox72 = new System.Windows.Forms.CheckBox();
            this.checkBox73 = new System.Windows.Forms.CheckBox();
            this.checkBox74 = new System.Windows.Forms.CheckBox();
            this.checkBox75 = new System.Windows.Forms.CheckBox();
            this.checkBox76 = new System.Windows.Forms.CheckBox();
            this.checkBox77 = new System.Windows.Forms.CheckBox();
            this.checkBox78 = new System.Windows.Forms.CheckBox();
            this.checkBox79 = new System.Windows.Forms.CheckBox();
            this.checkBox80 = new System.Windows.Forms.CheckBox();
            this.checkBox81 = new System.Windows.Forms.CheckBox();
            this.checkBox82 = new System.Windows.Forms.CheckBox();
            this.checkBox83 = new System.Windows.Forms.CheckBox();
            this.checkBox84 = new System.Windows.Forms.CheckBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.savedialog = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(558, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "_";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(585, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "X";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Location = new System.Drawing.Point(216, 66);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(329, 91);
            this.panel1.TabIndex = 2;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 39);
            this.label4.TabIndex = 0;
            this.label4.Text = "version 1.0:\r\n\r\n- first initial release";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(216, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Changelog:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.checkBox42);
            this.panel3.Controls.Add(this.checkBox41);
            this.panel3.Controls.Add(this.checkBox40);
            this.panel3.Controls.Add(this.checkBox39);
            this.panel3.Controls.Add(this.checkBox38);
            this.panel3.Controls.Add(this.checkBox37);
            this.panel3.Controls.Add(this.checkBox36);
            this.panel3.Controls.Add(this.checkBox35);
            this.panel3.Controls.Add(this.checkBox34);
            this.panel3.Controls.Add(this.checkBox33);
            this.panel3.Controls.Add(this.button5);
            this.panel3.Controls.Add(this.checkBox32);
            this.panel3.Controls.Add(this.checkBox31);
            this.panel3.Controls.Add(this.checkBox30);
            this.panel3.Controls.Add(this.checkBox29);
            this.panel3.Controls.Add(this.checkBox28);
            this.panel3.Controls.Add(this.checkBox27);
            this.panel3.Controls.Add(this.checkBox26);
            this.panel3.Controls.Add(this.checkBox25);
            this.panel3.Controls.Add(this.checkBox24);
            this.panel3.Controls.Add(this.checkBox23);
            this.panel3.Controls.Add(this.checkBox22);
            this.panel3.Controls.Add(this.checkBox21);
            this.panel3.Controls.Add(this.checkBox20);
            this.panel3.Controls.Add(this.checkBox19);
            this.panel3.Controls.Add(this.checkBox18);
            this.panel3.Controls.Add(this.checkBox17);
            this.panel3.Controls.Add(this.checkBox16);
            this.panel3.Controls.Add(this.checkBox15);
            this.panel3.Controls.Add(this.checkBox14);
            this.panel3.Controls.Add(this.checkBox13);
            this.panel3.Controls.Add(this.checkBox12);
            this.panel3.Controls.Add(this.checkBox11);
            this.panel3.Controls.Add(this.checkBox10);
            this.panel3.Controls.Add(this.checkBox9);
            this.panel3.Controls.Add(this.checkBox8);
            this.panel3.Controls.Add(this.checkBox7);
            this.panel3.Controls.Add(this.checkBox6);
            this.panel3.Controls.Add(this.checkBox5);
            this.panel3.Controls.Add(this.checkBox4);
            this.panel3.Controls.Add(this.checkBox3);
            this.panel3.Controls.Add(this.checkBox2);
            this.panel3.Controls.Add(this.checkBox1);
            this.panel3.Controls.Add(this.button4);
            this.panel3.Location = new System.Drawing.Point(24, 326);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(598, 204);
            this.panel3.TabIndex = 3;
            // 
            // checkBox42
            // 
            this.checkBox42.AutoSize = true;
            this.checkBox42.ForeColor = System.Drawing.Color.White;
            this.checkBox42.Location = new System.Drawing.Point(442, 126);
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new System.Drawing.Size(63, 17);
            this.checkBox42.TabIndex = 46;
            this.checkBox42.Text = "Trilobite";
            this.checkBox42.UseVisualStyleBackColor = true;
            // 
            // checkBox41
            // 
            this.checkBox41.AutoSize = true;
            this.checkBox41.ForeColor = System.Drawing.Color.White;
            this.checkBox41.Location = new System.Drawing.Point(362, 126);
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new System.Drawing.Size(79, 17);
            this.checkBox41.TabIndex = 45;
            this.checkBox41.Text = "Triceratops";
            this.checkBox41.UseVisualStyleBackColor = true;
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.ForeColor = System.Drawing.Color.White;
            this.checkBox40.Location = new System.Drawing.Point(286, 126);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(74, 17);
            this.checkBox40.TabIndex = 44;
            this.checkBox40.Text = "Titanoboa";
            this.checkBox40.UseVisualStyleBackColor = true;
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.ForeColor = System.Drawing.Color.White;
            this.checkBox39.Location = new System.Drawing.Point(195, 126);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(85, 17);
            this.checkBox39.TabIndex = 43;
            this.checkBox39.Text = "Stegosaurus";
            this.checkBox39.UseVisualStyleBackColor = true;
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.ForeColor = System.Drawing.Color.White;
            this.checkBox38.Location = new System.Drawing.Point(137, 126);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(53, 17);
            this.checkBox38.TabIndex = 42;
            this.checkBox38.Text = "Spino";
            this.checkBox38.UseVisualStyleBackColor = true;
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.ForeColor = System.Drawing.Color.White;
            this.checkBox37.Location = new System.Drawing.Point(77, 126);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(54, 17);
            this.checkBox37.TabIndex = 41;
            this.checkBox37.Text = "Sarco";
            this.checkBox37.UseVisualStyleBackColor = true;
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.ForeColor = System.Drawing.Color.White;
            this.checkBox36.Location = new System.Drawing.Point(10, 126);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(61, 17);
            this.checkBox36.TabIndex = 40;
            this.checkBox36.Text = "Salmon";
            this.checkBox36.UseVisualStyleBackColor = true;
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.ForeColor = System.Drawing.Color.White;
            this.checkBox35.Location = new System.Drawing.Point(482, 103);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(58, 17);
            this.checkBox35.TabIndex = 39;
            this.checkBox35.Text = "Raptor";
            this.checkBox35.UseVisualStyleBackColor = true;
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.ForeColor = System.Drawing.Color.White;
            this.checkBox34.Location = new System.Drawing.Point(415, 103);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(64, 17);
            this.checkBox34.TabIndex = 38;
            this.checkBox34.Text = "Purlovia";
            this.checkBox34.UseVisualStyleBackColor = true;
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.ForeColor = System.Drawing.Color.White;
            this.checkBox33.Location = new System.Drawing.Point(303, 103);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(106, 17);
            this.checkBox33.TabIndex = 37;
            this.checkBox33.Text = "Pulmonoscorpius";
            this.checkBox33.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.ForestGreen;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(434, 169);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 36;
            this.button5.Text = "select all";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.ForeColor = System.Drawing.Color.White;
            this.checkBox32.Location = new System.Drawing.Point(240, 103);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(62, 17);
            this.checkBox32.TabIndex = 35;
            this.checkBox32.Text = "Piranha";
            this.checkBox32.UseVisualStyleBackColor = true;
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.ForeColor = System.Drawing.Color.White;
            this.checkBox31.Location = new System.Drawing.Point(170, 103);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(68, 17);
            this.checkBox31.TabIndex = 34;
            this.checkBox31.Text = "Parasaur";
            this.checkBox31.UseVisualStyleBackColor = true;
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.ForeColor = System.Drawing.Color.White;
            this.checkBox30.Location = new System.Drawing.Point(66, 103);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(103, 17);
            this.checkBox30.TabIndex = 33;
            this.checkBox30.Text = "Paraceratherium";
            this.checkBox30.UseVisualStyleBackColor = true;
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.ForeColor = System.Drawing.Color.White;
            this.checkBox29.Location = new System.Drawing.Point(11, 103);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(47, 17);
            this.checkBox29.TabIndex = 32;
            this.checkBox29.Text = "Ovis";
            this.checkBox29.UseVisualStyleBackColor = true;
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.ForeColor = System.Drawing.Color.White;
            this.checkBox28.Location = new System.Drawing.Point(507, 80);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(49, 17);
            this.checkBox28.TabIndex = 31;
            this.checkBox28.Text = "Otter";
            this.checkBox28.UseVisualStyleBackColor = true;
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.ForeColor = System.Drawing.Color.White;
            this.checkBox27.Location = new System.Drawing.Point(426, 80);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(75, 17);
            this.checkBox27.TabIndex = 30;
            this.checkBox27.Text = "Moschops";
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.ForeColor = System.Drawing.Color.White;
            this.checkBox26.Location = new System.Drawing.Point(340, 80);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(80, 17);
            this.checkBox26.TabIndex = 29;
            this.checkBox26.Text = "Meganeura";
            this.checkBox26.UseVisualStyleBackColor = true;
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.ForeColor = System.Drawing.Color.White;
            this.checkBox25.Location = new System.Drawing.Point(242, 80);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(92, 17);
            this.checkBox25.TabIndex = 28;
            this.checkBox25.Text = "Megalosaurus";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.ForeColor = System.Drawing.Color.White;
            this.checkBox24.Location = new System.Drawing.Point(161, 80);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(75, 17);
            this.checkBox24.TabIndex = 27;
            this.checkBox24.Text = "Megalania";
            this.checkBox24.UseVisualStyleBackColor = true;
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.ForeColor = System.Drawing.Color.White;
            this.checkBox23.Location = new System.Drawing.Point(104, 80);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(56, 17);
            this.checkBox23.TabIndex = 26;
            this.checkBox23.Text = "Manta";
            this.checkBox23.UseVisualStyleBackColor = true;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.ForeColor = System.Drawing.Color.White;
            this.checkBox22.Location = new System.Drawing.Point(10, 80);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(85, 17);
            this.checkBox22.TabIndex = 25;
            this.checkBox22.Text = "Lystrosaurus";
            this.checkBox22.UseVisualStyleBackColor = true;
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.ForeColor = System.Drawing.Color.White;
            this.checkBox21.Location = new System.Drawing.Point(496, 56);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(77, 17);
            this.checkBox21.TabIndex = 24;
            this.checkBox21.Text = "Iguanodon";
            this.checkBox21.UseVisualStyleBackColor = true;
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.ForeColor = System.Drawing.Color.White;
            this.checkBox20.Location = new System.Drawing.Point(385, 56);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(103, 17);
            this.checkBox20.TabIndex = 23;
            this.checkBox20.Text = "Gigantopithecus";
            this.checkBox20.UseVisualStyleBackColor = true;
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.ForeColor = System.Drawing.Color.White;
            this.checkBox19.Location = new System.Drawing.Point(325, 56);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(56, 17);
            this.checkBox19.TabIndex = 22;
            this.checkBox19.Text = "Equus";
            this.checkBox19.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.ForeColor = System.Drawing.Color.White;
            this.checkBox18.Location = new System.Drawing.Point(237, 56);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(91, 17);
            this.checkBox18.TabIndex = 21;
            this.checkBox18.Text = "Electrophorus";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.ForeColor = System.Drawing.Color.White;
            this.checkBox17.Location = new System.Drawing.Point(149, 56);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(88, 17);
            this.checkBox17.TabIndex = 20;
            this.checkBox17.Text = "Dung_Beetle";
            this.checkBox17.UseVisualStyleBackColor = true;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.ForeColor = System.Drawing.Color.White;
            this.checkBox16.Location = new System.Drawing.Point(69, 56);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(80, 17);
            this.checkBox16.TabIndex = 19;
            this.checkBox16.Text = "Doedicurus";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.ForeColor = System.Drawing.Color.White;
            this.checkBox15.Location = new System.Drawing.Point(11, 57);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(52, 17);
            this.checkBox15.TabIndex = 18;
            this.checkBox15.Text = "Dodo";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.ForeColor = System.Drawing.Color.White;
            this.checkBox14.Location = new System.Drawing.Point(507, 34);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(70, 17);
            this.checkBox14.TabIndex = 17;
            this.checkBox14.Text = "Dire Bear";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.ForeColor = System.Drawing.Color.White;
            this.checkBox13.Location = new System.Drawing.Point(501, 11);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(79, 17);
            this.checkBox13.TabIndex = 16;
            this.checkBox13.Text = "Diplodocus";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.ForeColor = System.Drawing.Color.White;
            this.checkBox12.Location = new System.Drawing.Point(428, 34);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(81, 17);
            this.checkBox12.TabIndex = 15;
            this.checkBox12.Text = "Diplocaulus";
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.ForeColor = System.Drawing.Color.White;
            this.checkBox11.Location = new System.Drawing.Point(402, 11);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(89, 17);
            this.checkBox11.TabIndex = 14;
            this.checkBox11.Text = "Dimorphodon";
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.ForeColor = System.Drawing.Color.White;
            this.checkBox10.Location = new System.Drawing.Point(342, 34);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(80, 17);
            this.checkBox10.TabIndex = 13;
            this.checkBox10.Text = "Dimetrodon";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.ForeColor = System.Drawing.Color.White;
            this.checkBox9.Location = new System.Drawing.Point(337, 11);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(64, 17);
            this.checkBox9.TabIndex = 12;
            this.checkBox9.Text = "Cnidaria";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.ForeColor = System.Drawing.Color.White;
            this.checkBox8.Location = new System.Drawing.Point(247, 11);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(84, 17);
            this.checkBox8.TabIndex = 11;
            this.checkBox8.Text = "Carbonemys";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.ForeColor = System.Drawing.Color.White;
            this.checkBox7.Location = new System.Drawing.Point(259, 33);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(83, 17);
            this.checkBox7.TabIndex = 10;
            this.checkBox7.Text = "Carnotaurus";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.ForeColor = System.Drawing.Color.White;
            this.checkBox6.Location = new System.Drawing.Point(172, 34);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(79, 17);
            this.checkBox6.TabIndex = 9;
            this.checkBox6.Text = "Beelzebufo";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.ForeColor = System.Drawing.Color.White;
            this.checkBox5.Location = new System.Drawing.Point(172, 11);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(69, 17);
            this.checkBox5.TabIndex = 8;
            this.checkBox5.Text = "Baryonyx";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.ForeColor = System.Drawing.Color.White;
            this.checkBox4.Location = new System.Drawing.Point(84, 33);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(83, 17);
            this.checkBox4.TabIndex = 7;
            this.checkBox4.Text = "Arthropluera";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.ForeColor = System.Drawing.Color.White;
            this.checkBox3.Location = new System.Drawing.Point(96, 11);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(72, 17);
            this.checkBox3.TabIndex = 6;
            this.checkBox3.Text = "Anglerfish";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.ForeColor = System.Drawing.Color.White;
            this.checkBox2.Location = new System.Drawing.Point(10, 34);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(68, 17);
            this.checkBox2.TabIndex = 5;
            this.checkBox2.Text = "Achatina";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.ForeColor = System.Drawing.Color.White;
            this.checkBox1.Location = new System.Drawing.Point(10, 11);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(80, 17);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Coelacanth";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.ForestGreen;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(512, 169);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 3;
            this.button4.Text = "generate";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(203, 308);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(214, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Change specific entities to: Aberrant";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(183, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Save Location:";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.textBox2);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Location = new System.Drawing.Point(186, 203);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(271, 31);
            this.panel4.TabIndex = 6;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.ForestGreen;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(184, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "save location";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(58, 6);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(120, 20);
            this.textBox2.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(6, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Location:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(221, 546);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(203, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Change specific entities to: normal";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.checkBox43);
            this.panel6.Controls.Add(this.checkBox44);
            this.panel6.Controls.Add(this.checkBox45);
            this.panel6.Controls.Add(this.checkBox46);
            this.panel6.Controls.Add(this.checkBox47);
            this.panel6.Controls.Add(this.checkBox48);
            this.panel6.Controls.Add(this.checkBox49);
            this.panel6.Controls.Add(this.checkBox50);
            this.panel6.Controls.Add(this.checkBox51);
            this.panel6.Controls.Add(this.checkBox52);
            this.panel6.Controls.Add(this.button6);
            this.panel6.Controls.Add(this.checkBox53);
            this.panel6.Controls.Add(this.checkBox54);
            this.panel6.Controls.Add(this.checkBox55);
            this.panel6.Controls.Add(this.checkBox56);
            this.panel6.Controls.Add(this.checkBox57);
            this.panel6.Controls.Add(this.checkBox58);
            this.panel6.Controls.Add(this.checkBox59);
            this.panel6.Controls.Add(this.checkBox60);
            this.panel6.Controls.Add(this.checkBox61);
            this.panel6.Controls.Add(this.checkBox62);
            this.panel6.Controls.Add(this.checkBox63);
            this.panel6.Controls.Add(this.checkBox64);
            this.panel6.Controls.Add(this.checkBox65);
            this.panel6.Controls.Add(this.checkBox66);
            this.panel6.Controls.Add(this.checkBox67);
            this.panel6.Controls.Add(this.checkBox68);
            this.panel6.Controls.Add(this.checkBox69);
            this.panel6.Controls.Add(this.checkBox70);
            this.panel6.Controls.Add(this.checkBox71);
            this.panel6.Controls.Add(this.checkBox72);
            this.panel6.Controls.Add(this.checkBox73);
            this.panel6.Controls.Add(this.checkBox74);
            this.panel6.Controls.Add(this.checkBox75);
            this.panel6.Controls.Add(this.checkBox76);
            this.panel6.Controls.Add(this.checkBox77);
            this.panel6.Controls.Add(this.checkBox78);
            this.panel6.Controls.Add(this.checkBox79);
            this.panel6.Controls.Add(this.checkBox80);
            this.panel6.Controls.Add(this.checkBox81);
            this.panel6.Controls.Add(this.checkBox82);
            this.panel6.Controls.Add(this.checkBox83);
            this.panel6.Controls.Add(this.checkBox84);
            this.panel6.Controls.Add(this.button7);
            this.panel6.Location = new System.Drawing.Point(22, 574);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(598, 204);
            this.panel6.TabIndex = 47;
            // 
            // checkBox43
            // 
            this.checkBox43.AutoSize = true;
            this.checkBox43.ForeColor = System.Drawing.Color.White;
            this.checkBox43.Location = new System.Drawing.Point(442, 126);
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new System.Drawing.Size(63, 17);
            this.checkBox43.TabIndex = 46;
            this.checkBox43.Text = "Trilobite";
            this.checkBox43.UseVisualStyleBackColor = true;
            // 
            // checkBox44
            // 
            this.checkBox44.AutoSize = true;
            this.checkBox44.ForeColor = System.Drawing.Color.White;
            this.checkBox44.Location = new System.Drawing.Point(362, 126);
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new System.Drawing.Size(79, 17);
            this.checkBox44.TabIndex = 45;
            this.checkBox44.Text = "Triceratops";
            this.checkBox44.UseVisualStyleBackColor = true;
            // 
            // checkBox45
            // 
            this.checkBox45.AutoSize = true;
            this.checkBox45.ForeColor = System.Drawing.Color.White;
            this.checkBox45.Location = new System.Drawing.Point(286, 126);
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new System.Drawing.Size(74, 17);
            this.checkBox45.TabIndex = 44;
            this.checkBox45.Text = "Titanoboa";
            this.checkBox45.UseVisualStyleBackColor = true;
            // 
            // checkBox46
            // 
            this.checkBox46.AutoSize = true;
            this.checkBox46.ForeColor = System.Drawing.Color.White;
            this.checkBox46.Location = new System.Drawing.Point(195, 126);
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new System.Drawing.Size(85, 17);
            this.checkBox46.TabIndex = 43;
            this.checkBox46.Text = "Stegosaurus";
            this.checkBox46.UseVisualStyleBackColor = true;
            // 
            // checkBox47
            // 
            this.checkBox47.AutoSize = true;
            this.checkBox47.ForeColor = System.Drawing.Color.White;
            this.checkBox47.Location = new System.Drawing.Point(137, 126);
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new System.Drawing.Size(53, 17);
            this.checkBox47.TabIndex = 42;
            this.checkBox47.Text = "Spino";
            this.checkBox47.UseVisualStyleBackColor = true;
            // 
            // checkBox48
            // 
            this.checkBox48.AutoSize = true;
            this.checkBox48.ForeColor = System.Drawing.Color.White;
            this.checkBox48.Location = new System.Drawing.Point(77, 126);
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new System.Drawing.Size(54, 17);
            this.checkBox48.TabIndex = 41;
            this.checkBox48.Text = "Sarco";
            this.checkBox48.UseVisualStyleBackColor = true;
            // 
            // checkBox49
            // 
            this.checkBox49.AutoSize = true;
            this.checkBox49.ForeColor = System.Drawing.Color.White;
            this.checkBox49.Location = new System.Drawing.Point(10, 126);
            this.checkBox49.Name = "checkBox49";
            this.checkBox49.Size = new System.Drawing.Size(61, 17);
            this.checkBox49.TabIndex = 40;
            this.checkBox49.Text = "Salmon";
            this.checkBox49.UseVisualStyleBackColor = true;
            // 
            // checkBox50
            // 
            this.checkBox50.AutoSize = true;
            this.checkBox50.ForeColor = System.Drawing.Color.White;
            this.checkBox50.Location = new System.Drawing.Point(482, 103);
            this.checkBox50.Name = "checkBox50";
            this.checkBox50.Size = new System.Drawing.Size(58, 17);
            this.checkBox50.TabIndex = 39;
            this.checkBox50.Text = "Raptor";
            this.checkBox50.UseVisualStyleBackColor = true;
            // 
            // checkBox51
            // 
            this.checkBox51.AutoSize = true;
            this.checkBox51.ForeColor = System.Drawing.Color.White;
            this.checkBox51.Location = new System.Drawing.Point(415, 103);
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new System.Drawing.Size(64, 17);
            this.checkBox51.TabIndex = 38;
            this.checkBox51.Text = "Purlovia";
            this.checkBox51.UseVisualStyleBackColor = true;
            // 
            // checkBox52
            // 
            this.checkBox52.AutoSize = true;
            this.checkBox52.ForeColor = System.Drawing.Color.White;
            this.checkBox52.Location = new System.Drawing.Point(303, 103);
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new System.Drawing.Size(106, 17);
            this.checkBox52.TabIndex = 37;
            this.checkBox52.Text = "Pulmonoscorpius";
            this.checkBox52.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.ForestGreen;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(434, 169);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 36;
            this.button6.Text = "select all";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // checkBox53
            // 
            this.checkBox53.AutoSize = true;
            this.checkBox53.ForeColor = System.Drawing.Color.White;
            this.checkBox53.Location = new System.Drawing.Point(240, 103);
            this.checkBox53.Name = "checkBox53";
            this.checkBox53.Size = new System.Drawing.Size(62, 17);
            this.checkBox53.TabIndex = 35;
            this.checkBox53.Text = "Piranha";
            this.checkBox53.UseVisualStyleBackColor = true;
            // 
            // checkBox54
            // 
            this.checkBox54.AutoSize = true;
            this.checkBox54.ForeColor = System.Drawing.Color.White;
            this.checkBox54.Location = new System.Drawing.Point(170, 103);
            this.checkBox54.Name = "checkBox54";
            this.checkBox54.Size = new System.Drawing.Size(68, 17);
            this.checkBox54.TabIndex = 34;
            this.checkBox54.Text = "Parasaur";
            this.checkBox54.UseVisualStyleBackColor = true;
            // 
            // checkBox55
            // 
            this.checkBox55.AutoSize = true;
            this.checkBox55.ForeColor = System.Drawing.Color.White;
            this.checkBox55.Location = new System.Drawing.Point(66, 103);
            this.checkBox55.Name = "checkBox55";
            this.checkBox55.Size = new System.Drawing.Size(103, 17);
            this.checkBox55.TabIndex = 33;
            this.checkBox55.Text = "Paraceratherium";
            this.checkBox55.UseVisualStyleBackColor = true;
            // 
            // checkBox56
            // 
            this.checkBox56.AutoSize = true;
            this.checkBox56.ForeColor = System.Drawing.Color.White;
            this.checkBox56.Location = new System.Drawing.Point(11, 103);
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new System.Drawing.Size(47, 17);
            this.checkBox56.TabIndex = 32;
            this.checkBox56.Text = "Ovis";
            this.checkBox56.UseVisualStyleBackColor = true;
            // 
            // checkBox57
            // 
            this.checkBox57.AutoSize = true;
            this.checkBox57.ForeColor = System.Drawing.Color.White;
            this.checkBox57.Location = new System.Drawing.Point(507, 80);
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new System.Drawing.Size(49, 17);
            this.checkBox57.TabIndex = 31;
            this.checkBox57.Text = "Otter";
            this.checkBox57.UseVisualStyleBackColor = true;
            // 
            // checkBox58
            // 
            this.checkBox58.AutoSize = true;
            this.checkBox58.ForeColor = System.Drawing.Color.White;
            this.checkBox58.Location = new System.Drawing.Point(426, 80);
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new System.Drawing.Size(75, 17);
            this.checkBox58.TabIndex = 30;
            this.checkBox58.Text = "Moschops";
            this.checkBox58.UseVisualStyleBackColor = true;
            // 
            // checkBox59
            // 
            this.checkBox59.AutoSize = true;
            this.checkBox59.ForeColor = System.Drawing.Color.White;
            this.checkBox59.Location = new System.Drawing.Point(340, 80);
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new System.Drawing.Size(80, 17);
            this.checkBox59.TabIndex = 29;
            this.checkBox59.Text = "Meganeura";
            this.checkBox59.UseVisualStyleBackColor = true;
            // 
            // checkBox60
            // 
            this.checkBox60.AutoSize = true;
            this.checkBox60.ForeColor = System.Drawing.Color.White;
            this.checkBox60.Location = new System.Drawing.Point(242, 80);
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new System.Drawing.Size(92, 17);
            this.checkBox60.TabIndex = 28;
            this.checkBox60.Text = "Megalosaurus";
            this.checkBox60.UseVisualStyleBackColor = true;
            // 
            // checkBox61
            // 
            this.checkBox61.AutoSize = true;
            this.checkBox61.ForeColor = System.Drawing.Color.White;
            this.checkBox61.Location = new System.Drawing.Point(161, 80);
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new System.Drawing.Size(75, 17);
            this.checkBox61.TabIndex = 27;
            this.checkBox61.Text = "Megalania";
            this.checkBox61.UseVisualStyleBackColor = true;
            // 
            // checkBox62
            // 
            this.checkBox62.AutoSize = true;
            this.checkBox62.ForeColor = System.Drawing.Color.White;
            this.checkBox62.Location = new System.Drawing.Point(104, 80);
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new System.Drawing.Size(56, 17);
            this.checkBox62.TabIndex = 26;
            this.checkBox62.Text = "Manta";
            this.checkBox62.UseVisualStyleBackColor = true;
            // 
            // checkBox63
            // 
            this.checkBox63.AutoSize = true;
            this.checkBox63.ForeColor = System.Drawing.Color.White;
            this.checkBox63.Location = new System.Drawing.Point(10, 80);
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new System.Drawing.Size(85, 17);
            this.checkBox63.TabIndex = 25;
            this.checkBox63.Text = "Lystrosaurus";
            this.checkBox63.UseVisualStyleBackColor = true;
            // 
            // checkBox64
            // 
            this.checkBox64.AutoSize = true;
            this.checkBox64.ForeColor = System.Drawing.Color.White;
            this.checkBox64.Location = new System.Drawing.Point(495, 56);
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new System.Drawing.Size(77, 17);
            this.checkBox64.TabIndex = 24;
            this.checkBox64.Text = "Iguanodon";
            this.checkBox64.UseVisualStyleBackColor = true;
            // 
            // checkBox65
            // 
            this.checkBox65.AutoSize = true;
            this.checkBox65.ForeColor = System.Drawing.Color.White;
            this.checkBox65.Location = new System.Drawing.Point(384, 56);
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new System.Drawing.Size(103, 17);
            this.checkBox65.TabIndex = 23;
            this.checkBox65.Text = "Gigantopithecus";
            this.checkBox65.UseVisualStyleBackColor = true;
            // 
            // checkBox66
            // 
            this.checkBox66.AutoSize = true;
            this.checkBox66.ForeColor = System.Drawing.Color.White;
            this.checkBox66.Location = new System.Drawing.Point(325, 56);
            this.checkBox66.Name = "checkBox66";
            this.checkBox66.Size = new System.Drawing.Size(56, 17);
            this.checkBox66.TabIndex = 22;
            this.checkBox66.Text = "Equus";
            this.checkBox66.UseVisualStyleBackColor = true;
            // 
            // checkBox67
            // 
            this.checkBox67.AutoSize = true;
            this.checkBox67.ForeColor = System.Drawing.Color.White;
            this.checkBox67.Location = new System.Drawing.Point(235, 56);
            this.checkBox67.Name = "checkBox67";
            this.checkBox67.Size = new System.Drawing.Size(91, 17);
            this.checkBox67.TabIndex = 21;
            this.checkBox67.Text = "Electrophorus";
            this.checkBox67.UseVisualStyleBackColor = true;
            // 
            // checkBox68
            // 
            this.checkBox68.AutoSize = true;
            this.checkBox68.ForeColor = System.Drawing.Color.White;
            this.checkBox68.Location = new System.Drawing.Point(149, 56);
            this.checkBox68.Name = "checkBox68";
            this.checkBox68.Size = new System.Drawing.Size(85, 17);
            this.checkBox68.TabIndex = 20;
            this.checkBox68.Text = "Dung Beetle";
            this.checkBox68.UseVisualStyleBackColor = true;
            // 
            // checkBox69
            // 
            this.checkBox69.AutoSize = true;
            this.checkBox69.ForeColor = System.Drawing.Color.White;
            this.checkBox69.Location = new System.Drawing.Point(69, 56);
            this.checkBox69.Name = "checkBox69";
            this.checkBox69.Size = new System.Drawing.Size(80, 17);
            this.checkBox69.TabIndex = 19;
            this.checkBox69.Text = "Doedicurus";
            this.checkBox69.UseVisualStyleBackColor = true;
            // 
            // checkBox70
            // 
            this.checkBox70.AutoSize = true;
            this.checkBox70.ForeColor = System.Drawing.Color.White;
            this.checkBox70.Location = new System.Drawing.Point(11, 57);
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new System.Drawing.Size(52, 17);
            this.checkBox70.TabIndex = 18;
            this.checkBox70.Text = "Dodo";
            this.checkBox70.UseVisualStyleBackColor = true;
            // 
            // checkBox71
            // 
            this.checkBox71.AutoSize = true;
            this.checkBox71.ForeColor = System.Drawing.Color.White;
            this.checkBox71.Location = new System.Drawing.Point(507, 34);
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new System.Drawing.Size(70, 17);
            this.checkBox71.TabIndex = 17;
            this.checkBox71.Text = "Dire Bear";
            this.checkBox71.UseVisualStyleBackColor = true;
            // 
            // checkBox72
            // 
            this.checkBox72.AutoSize = true;
            this.checkBox72.ForeColor = System.Drawing.Color.White;
            this.checkBox72.Location = new System.Drawing.Point(501, 11);
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new System.Drawing.Size(79, 17);
            this.checkBox72.TabIndex = 16;
            this.checkBox72.Text = "Diplodocus";
            this.checkBox72.UseVisualStyleBackColor = true;
            // 
            // checkBox73
            // 
            this.checkBox73.AutoSize = true;
            this.checkBox73.ForeColor = System.Drawing.Color.White;
            this.checkBox73.Location = new System.Drawing.Point(428, 34);
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new System.Drawing.Size(81, 17);
            this.checkBox73.TabIndex = 15;
            this.checkBox73.Text = "Diplocaulus";
            this.checkBox73.UseVisualStyleBackColor = true;
            // 
            // checkBox74
            // 
            this.checkBox74.AutoSize = true;
            this.checkBox74.ForeColor = System.Drawing.Color.White;
            this.checkBox74.Location = new System.Drawing.Point(402, 11);
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new System.Drawing.Size(89, 17);
            this.checkBox74.TabIndex = 14;
            this.checkBox74.Text = "Dimorphodon";
            this.checkBox74.UseVisualStyleBackColor = true;
            // 
            // checkBox75
            // 
            this.checkBox75.AutoSize = true;
            this.checkBox75.ForeColor = System.Drawing.Color.White;
            this.checkBox75.Location = new System.Drawing.Point(342, 34);
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new System.Drawing.Size(80, 17);
            this.checkBox75.TabIndex = 13;
            this.checkBox75.Text = "Dimetrodon";
            this.checkBox75.UseVisualStyleBackColor = true;
            // 
            // checkBox76
            // 
            this.checkBox76.AutoSize = true;
            this.checkBox76.ForeColor = System.Drawing.Color.White;
            this.checkBox76.Location = new System.Drawing.Point(337, 11);
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new System.Drawing.Size(64, 17);
            this.checkBox76.TabIndex = 12;
            this.checkBox76.Text = "Cnidaria";
            this.checkBox76.UseVisualStyleBackColor = true;
            // 
            // checkBox77
            // 
            this.checkBox77.AutoSize = true;
            this.checkBox77.ForeColor = System.Drawing.Color.White;
            this.checkBox77.Location = new System.Drawing.Point(247, 11);
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new System.Drawing.Size(84, 17);
            this.checkBox77.TabIndex = 11;
            this.checkBox77.Text = "Carbonemys";
            this.checkBox77.UseVisualStyleBackColor = true;
            // 
            // checkBox78
            // 
            this.checkBox78.AutoSize = true;
            this.checkBox78.ForeColor = System.Drawing.Color.White;
            this.checkBox78.Location = new System.Drawing.Point(259, 33);
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new System.Drawing.Size(83, 17);
            this.checkBox78.TabIndex = 10;
            this.checkBox78.Text = "Carnotaurus";
            this.checkBox78.UseVisualStyleBackColor = true;
            // 
            // checkBox79
            // 
            this.checkBox79.AutoSize = true;
            this.checkBox79.ForeColor = System.Drawing.Color.White;
            this.checkBox79.Location = new System.Drawing.Point(172, 34);
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new System.Drawing.Size(79, 17);
            this.checkBox79.TabIndex = 9;
            this.checkBox79.Text = "Beelzebufo";
            this.checkBox79.UseVisualStyleBackColor = true;
            // 
            // checkBox80
            // 
            this.checkBox80.AutoSize = true;
            this.checkBox80.ForeColor = System.Drawing.Color.White;
            this.checkBox80.Location = new System.Drawing.Point(172, 11);
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new System.Drawing.Size(69, 17);
            this.checkBox80.TabIndex = 8;
            this.checkBox80.Text = "Baryonyx";
            this.checkBox80.UseVisualStyleBackColor = true;
            // 
            // checkBox81
            // 
            this.checkBox81.AutoSize = true;
            this.checkBox81.ForeColor = System.Drawing.Color.White;
            this.checkBox81.Location = new System.Drawing.Point(84, 33);
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new System.Drawing.Size(83, 17);
            this.checkBox81.TabIndex = 7;
            this.checkBox81.Text = "Arthropluera";
            this.checkBox81.UseVisualStyleBackColor = true;
            // 
            // checkBox82
            // 
            this.checkBox82.AutoSize = true;
            this.checkBox82.ForeColor = System.Drawing.Color.White;
            this.checkBox82.Location = new System.Drawing.Point(96, 11);
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new System.Drawing.Size(72, 17);
            this.checkBox82.TabIndex = 6;
            this.checkBox82.Text = "Anglerfish";
            this.checkBox82.UseVisualStyleBackColor = true;
            // 
            // checkBox83
            // 
            this.checkBox83.AutoSize = true;
            this.checkBox83.ForeColor = System.Drawing.Color.White;
            this.checkBox83.Location = new System.Drawing.Point(10, 34);
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new System.Drawing.Size(68, 17);
            this.checkBox83.TabIndex = 5;
            this.checkBox83.Text = "Achatina";
            this.checkBox83.UseVisualStyleBackColor = true;
            // 
            // checkBox84
            // 
            this.checkBox84.AutoSize = true;
            this.checkBox84.ForeColor = System.Drawing.Color.White;
            this.checkBox84.Location = new System.Drawing.Point(10, 11);
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new System.Drawing.Size(80, 17);
            this.checkBox84.TabIndex = 4;
            this.checkBox84.Text = "Coelacanth";
            this.checkBox84.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.ForestGreen;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Green;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(512, 169);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 3;
            this.button7.Text = "generate";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(236, 251);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(168, 29);
            this.label10.TabIndex = 48;
            this.label10.Text = "Modify ini file";
            // 
            // savedialog
            // 
            this.savedialog.DefaultExt = "txt";
            this.savedialog.Filter = "txt|";
            this.savedialog.FileOk += new System.ComponentModel.CancelEventHandler(this.savedialog_FileOk);
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::ArkCharManager.Properties.Resources.test4;
            this.ClientSize = new System.Drawing.Size(640, 800);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Window";
            this.Opacity = 0D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.Load += new System.EventHandler(this.Window_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox38;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.CheckBox checkBox41;
        private System.Windows.Forms.CheckBox checkBox42;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.CheckBox checkBox43;
        private System.Windows.Forms.CheckBox checkBox44;
        private System.Windows.Forms.CheckBox checkBox45;
        private System.Windows.Forms.CheckBox checkBox46;
        private System.Windows.Forms.CheckBox checkBox47;
        private System.Windows.Forms.CheckBox checkBox48;
        private System.Windows.Forms.CheckBox checkBox49;
        private System.Windows.Forms.CheckBox checkBox50;
        private System.Windows.Forms.CheckBox checkBox51;
        private System.Windows.Forms.CheckBox checkBox52;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.CheckBox checkBox53;
        private System.Windows.Forms.CheckBox checkBox54;
        private System.Windows.Forms.CheckBox checkBox55;
        private System.Windows.Forms.CheckBox checkBox56;
        private System.Windows.Forms.CheckBox checkBox57;
        private System.Windows.Forms.CheckBox checkBox58;
        private System.Windows.Forms.CheckBox checkBox59;
        private System.Windows.Forms.CheckBox checkBox60;
        private System.Windows.Forms.CheckBox checkBox61;
        private System.Windows.Forms.CheckBox checkBox62;
        private System.Windows.Forms.CheckBox checkBox63;
        private System.Windows.Forms.CheckBox checkBox64;
        private System.Windows.Forms.CheckBox checkBox65;
        private System.Windows.Forms.CheckBox checkBox66;
        private System.Windows.Forms.CheckBox checkBox67;
        private System.Windows.Forms.CheckBox checkBox68;
        private System.Windows.Forms.CheckBox checkBox69;
        private System.Windows.Forms.CheckBox checkBox70;
        private System.Windows.Forms.CheckBox checkBox71;
        private System.Windows.Forms.CheckBox checkBox72;
        private System.Windows.Forms.CheckBox checkBox73;
        private System.Windows.Forms.CheckBox checkBox74;
        private System.Windows.Forms.CheckBox checkBox75;
        private System.Windows.Forms.CheckBox checkBox76;
        private System.Windows.Forms.CheckBox checkBox77;
        private System.Windows.Forms.CheckBox checkBox78;
        private System.Windows.Forms.CheckBox checkBox79;
        private System.Windows.Forms.CheckBox checkBox80;
        private System.Windows.Forms.CheckBox checkBox81;
        private System.Windows.Forms.CheckBox checkBox82;
        private System.Windows.Forms.CheckBox checkBox83;
        private System.Windows.Forms.CheckBox checkBox84;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.SaveFileDialog savedialog;
    }
}

