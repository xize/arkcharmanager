﻿using ArkCharManager.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArkCharManager
{
    public partial class Window : Form
    {

        private Replacer replacer;

        public Window()
        {
            InitializeComponent();
            panel1.BackColor = Color.FromArgb(70, Color.Black);
            panel3.BackColor = Color.FromArgb(70, Color.Black);
            panel4.BackColor = Color.FromArgb(70, Color.Black);
            panel6.BackColor = Color.FromArgb(70, Color.Black);
            this.Icon = Resources.JwOKd_IziNwuvKM6Ke58IayJlWJFARGftLZYrUAlsow;
            label2.MouseEnter += new EventHandler(label2mouseenter);
            label2.MouseLeave += new EventHandler(label2mouseexit);
            this.MouseDown += new MouseEventHandler(window_MouseDown);
            this.replacer = new Replacer(this);
            FadeIn(this, 20);
        }

        private async void FadeIn(Form o, int interval = 80)
        {
            //Object is not fully invisible. Fade it in
            while (o.Opacity < 1.0)
            {
                await Task.Delay(interval);
                o.Opacity += 0.05;
            }
            o.Opacity = 1; //make fully visible       
        }

        private async void FadeOut(Form o, int interval = 80)
        {
            //Object is fully visible. Fade it out
            while (o.Opacity > 0.0)
            {
                await Task.Delay(interval);
                o.Opacity -= 0.05;
            }
            o.Opacity = 0; //make fully invisible       
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void window_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void label2mouseexit(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Bold);
        }

        private void label2mouseenter(object sender, EventArgs e)
        {
            label2.Font = new Font(label2.Font, FontStyle.Regular);
        }

        private void Window_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            panel3.Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            panel6.Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(button5.Text == "select all")
            {
                foreach(Control c in button5.Parent.Controls)
                {
                    if(c is CheckBox)
                    {
                        ((CheckBox)c).Checked = true;
                    }
                }
                button5.Text = "de-select all";
            } else
            {
                foreach (Control c in button5.Parent.Controls)
                {
                    if (c is CheckBox)
                    {
                        ((CheckBox)c).Checked = false;
                    }
                }
                button5.Text = "select all";
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (button6.Text == "select all")
            {
                foreach (Control c in button6.Parent.Controls)
                {
                    if (c is CheckBox)
                    {
                        ((CheckBox)c).Checked = true;
                    }
                }
                button6.Text = "de-select all";
            }
            else
            {
                foreach (Control c in button6.Parent.Controls)
                {
                    if (c is CheckBox)
                    {
                        ((CheckBox)c).Checked = false;
                    }
                }
                button6.Text = "select all";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult r = savedialog.ShowDialog();
            if(r == DialogResult.OK)
            {
                textBox2.Text = savedialog.FileName;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //first check if any save file is selected
            if(savedialog.FileName == "")
            {
                MessageBox.Show("you have no save file selected!", "error no file selected!");
                return;
            }

            button4.Enabled = false;

            //first loop through all controls and get the checkboxes to get the correct replacements
            List<string> replacements = new List<string>();
            foreach (Control c in button4.Parent.Controls)
            {
                if(c is CheckBox)
                {
                    CheckBox box = (CheckBox)c;
                    if(box.Checked)
                    {
                        replacements.Add(box.Text.Replace(" ", "_").ToLower());
                    }
                }
            }

            if(replacements.Count == 0)
            {
                MessageBox.Show("you have nothing selected to replace!", "nothing selected!");
                button4.Enabled = true;
                return;
            }

            //read lines to get the correct entities.
            //string newtext = "";
            StreamWriter fw = File.CreateText(savedialog.FileName);
            
            foreach(string r in replacements)
            {
                fw.WriteLine(replacer.SetLineToAberrant(r));
            }

            fw.Flush();
            fw.Close();
            MessageBox.Show("done!", "success");
            button4.Enabled = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //first check if any save file is selected
            if (savedialog.FileName == "")
            {
                MessageBox.Show("you have no save file selected!", "error no file selected!");
                return;
            }

            button7.Enabled = false;

            //first loop through all controls and get the checkboxes to get the correct replacements
            List<string> replacements = new List<string>();
            foreach (Control c in button7.Parent.Controls)
            {
                if (c is CheckBox)
                {
                    CheckBox box = (CheckBox)c;
                    if (box.Checked)
                    {
                        replacements.Add(box.Text.Replace(" ", "_").ToLower());
                    }
                }
            }

            if (replacements.Count == 0)
            {
                MessageBox.Show("you have nothing selected to replace!", "nothing selected!");
                button7.Enabled = true;
                return;
            }

            //read lines to get the correct entities.
            //string newtext = "";
            StreamWriter fw = File.CreateText(savedialog.FileName);

            foreach (string r in replacements)
            {
                fw.WriteLine(replacer.SetLineToNormal(r));
            }

            fw.Flush();
            fw.Close();

            MessageBox.Show("done!", "success");
            button7.Enabled = true;
        }

        private void savedialog_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
