﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ArkCharManager
{
    class Replacer
    {

        private Window window;

        private Dictionary<string, Dictionary<string, string>> abstractdata = new Dictionary<string, Dictionary<string, string>>()
        {
            // 
            // Mapping is Key lowercase the name of the entity, then the values key and value are actually both values in case sensitive format.
            //
            {"coelacanth", new Dictionary<string, string>() {
                { "Coel_Character_BP_Aberrant_C", "Coel_Character_BP_C" }
            }},
            {"achatina", new Dictionary<string, string>() {
                { "Achatina_Character_BP_Aberrant_C", "Achatina_Character_BP_C" }
            }},
            {"anglerfish", new Dictionary<string, string>() {
                { "Angler_Character_BP_Aberrant_C", "Angler_Character_BP_C" }
            }},
            {"ankylosaurus", new Dictionary<string, string>() {
                { "Ankylo_Character_BP_Aberrant_C", "Ankylo_Character_BP_C" }
            }},
            {"araneo", new Dictionary<string, string>() {
                { "SpiderS_Character_BP_Aberrant_C", "SpiderS_Character_BP_C" }
            }},
            {"arthropluera", new Dictionary<string, string>() {
                { "Arthro_Character_BP_Aberrant_C", "Arthro_Character_BP_C" }
            }},
            {"baryonyx", new Dictionary<string, string>() {
                { "Baryonyx_Character_BP_Aberrant_C", "Baryonyx_Character_BP_C" }
            }},
            {"beelzebufo", new Dictionary<string, string>() {
                { "Toad_Character_BP_Aberrant_C", "Toad_Character_BP_C" }
            }},
            {"carbonemys", new Dictionary<string, string>() {
                { "Turtle_Character_BP_Aberrant_C", "Turtle_Character_BP_C" }
            }},
            {"carnotaurus", new Dictionary<string, string>() {
                { "Carno_Character_BP_Aberrant_C", "Carno_Character_BP_C" }
            }},
            {"cnidaria", new Dictionary<string, string>() {
                { "Cnidaria_Character_BP_Aberrant_C", "Cnidaria_Character_BP_C" }
            }},
            {"dimetrodon", new Dictionary<string, string>() {
                { "Dimetro_Character_BP_Aberrant_C", "Dimetro_Character_BP_C" }
            }},
            {"dimorphodon", new Dictionary<string, string>() {
                { "Dimorph_Character_BP_Aberrant_C", "Dimorph_Character_BP_C" }
            }},
            {"diplocaulus", new Dictionary<string, string>() {
                { "Diplocaulus_Character_BP_Aberrant_C", "Diplocaulus_Character_BP_C" }
            }},
            {"diplodocus", new Dictionary<string, string>() {
                { "Diplodocus_Character_BP_Aberrant_C", "Diplodocus_Character_BP_C" }
            }},
            //TODO: make sure dire_bear is not getting forgotten by parsing!
            {"dire_bear", new Dictionary<string, string>() {
                { "Direbear_Character_BP_Aberrant_C", "Direbear_Character_BP_C" }
            }},
            {"dodo", new Dictionary<string, string>() {
                { "Dodo_Character_BP_Aberrant_C", "Dodo_Character_BP_C" }
            }},
             {"doedicurus", new Dictionary<string, string>() {
                { "Doed_Character_BP_Aberrant_C", "Doed_Character_BP_C" }
            }},
             //TODO: make sure dung_beetle is not getting forgotten by parsing!
            {"dung_beetle", new Dictionary<string, string>() {
                { "DungBeetle_Character_BP_Aberrant_C", "DungBeetle_Character_BP_C" }
            }},
            {"electrophorus", new Dictionary<string, string>() {
                { "Eel_Character_BP_Aberrant_C", "Eel_Character_BP_C" }
            }},
            {"equus", new Dictionary<string, string>() {
                { "Equus_Character_BP_Aberrant_C", "Equus_Character_BP_C" }
            }},
            {"gigantopithecus", new Dictionary<string, string>() {
                { "Bigfoot_Character_BP_Aberrant_C", "Bigfoot_Character_BP_C" }
            }},
            {"iguanodon", new Dictionary<string, string>() {
                { "Iguanodon_Character_BP_Aberrant_C", "Iguanodon_Character_BP_C" }
            }},
            {"lystrosaurus", new Dictionary<string, string>() {
                { "Lystro_Character_BP_Aberrant_C", "Lystro_Character_BP_C" }
            }},
            {"manta", new Dictionary<string, string>() {
                { "Manta_Character_BP_Aberrant_C", "Manta_Character_BP_C" }
            }},
            {"megalania", new Dictionary<string, string>() {
                { "Megalania_Character_BP_Aberrant_C", "Megalania_Character_BP_C" }
            }},
            {"megalosaurus", new Dictionary<string, string>() {
                { "Megalosaurus_Character_BP_Aberrant_C", "Megalosaurus_Character_BP_C" }
            }},
            {"meganeura", new Dictionary<string, string>() {
                { "Dragonfly_Character_BP_Aberrant_C", "Dragonfly_Character_BP_C" }
            }},
            {"moschops", new Dictionary<string, string>() {
                { "Moschops_Character_BP_Aberrant_C", "Moschops_Character_BP_C" }
            }},
            {"otter", new Dictionary<string, string>() {
                { "Otter_Character_BP_Aberrant_C", "Otter_Character_BP_C" }
            }},
            {"ovis", new Dictionary<string, string>() {
                { "Sheep_Character_BP_Aberrant_C", "Sheep_Character_BP_C" }
            }},
            {"paraceratherium", new Dictionary<string, string>() {
                { "Paracer_Character_BP_Aberrant_C", "Paracer_Character_BP_C" }
            }},
            {"parasaur", new Dictionary<string, string>() {
                { "Para_Character_BP_Aberrant_C", "Para_Character_BP_C" }
            }},
            {"piranha", new Dictionary<string, string>() {
                { "Piranha_Character_BP_Aberrant_C", "Piranha_Character_BP_C" }
            }},
            {"pulmonoscorpius", new Dictionary<string, string>() {
                { "Scorpion_Character_BP_Aberrant_C", "Scorpion_Character_BP_C" }
            }},
            {"purlovia", new Dictionary<string, string>() {
                { "Purlovia_Character_BP_Aberrant_C", "Purlovia_Character_BP_C" }
            }},
            {"raptor", new Dictionary<string, string>() {
                { "Raptor_Character_BP_Aberrant_C", "Raptor_Character_BP_C" }
            }},
            {"salmon", new Dictionary<string, string>() {
                { "Salmon_Character_BP_Aberrant_C", "Salmon_Character_BP_C" }
            }},
            {"sarco", new Dictionary<string, string>() {
                { "Sarco_Character_BP_Aberrant_C", "Sarco_Character_BP_C" }
            }},
            {"spino", new Dictionary<string, string>() {
                { "Spino_Character_BP_Aberrant_C", "Spino_Character_BP_C" }
            }},
            {"stegosaurus", new Dictionary<string, string>() {
                { "Stego_Character_BP_Aberrant_C", "Stego_Character_BP_C" }
            }},
            {"titanoboa", new Dictionary<string, string>() {
                { "BoaFrill_Character_BP_Aberrant_C", "BoaFrill_Character_BP_C" }
            }},
            {"triceratops", new Dictionary<string, string>() {
                { "Trike_Character_BP_Aberrant_C", "Trike_Character_BP_C" }
            }},
            {"trilobite", new Dictionary<string, string>() {
                { "Trilobite_Character_BP_Aberrant_C", "Trilobite_Character_BP_C" }
            }},
        };

        public Replacer(Window window)
        {
            this.window = window;
        }

        public string SetLineToAberrant(string line)
        {
            line = this.FormatLine(line);
            if(!abstractdata.ContainsKey(line))
            {
                MessageBox.Show("if you see this message please contact the developer, this message only shows when there is a mismatch with a entity name or its spelled incorrectly!\nname: "+line+"\nthe format is lowercase named where spaces are replaced by _ please lookup wiki regarding namings!", "error: mismatch either regex or wrong spelled");
                return line;
            }
            Dictionary<string, string> data = abstractdata[line];

            string key = data.Keys.First();
            string value = data.Values.First();

            line = "NPCReplacements = (FromClassName = \""+value+ "\",ToClassName=\""+key+"\")";
            return line;
        }

        public string SetLineToNormal(string line)
        {
            line = this.FormatLine(line);
            if (!abstractdata.ContainsKey(line))
            {
                MessageBox.Show("if you see this message please contact the developer, this message only shows when there is a mismatch with a entity name or its spelled incorrectly!\nname: " + line + "\nthe format is lowercase named where spaces are replaced by _ please lookup wiki regarding namings!", "error: mismatch either regex or wrong spelled");
                return line;
            }
            Dictionary<string, string> data = abstractdata[line];

            string key = data.Keys.First();
            string value = data.Values.First();

            line = "NPCReplacements = (FromClassName = \"" + key + "\",ToClassName=\"" + value + "\")";
            return line;
        }

        private string FormatLine(string d)
        {
            return d.Replace(" ", "_").ToLower();
        }

        private bool IsAberrant(string d)
        {
            if(d.Contains("Aberrant"))
            {
                return true;
            }
            return false;
        }

    }
}
